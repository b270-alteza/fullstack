import React from 'react'
import {Fragment} from 'react'
import Banner from '../components/Banner'

export default function NoPage() {
    return (
        <Fragment>
            <Banner
                title = "Page Not found"
                subtitle = { <span>Go back to the <a href="/">homepage</a></span> } 
            />
        </Fragment>
    )
}